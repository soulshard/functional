﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static System.Linq.Enumerable;
using static System.Math;

namespace basePlayground {

  public struct PixelInfo {
    public readonly int x, y;
    public readonly Color c;
    public PixelInfo(int x, int y, Color c) {
      this.x = x;
      this.y = y;
      this.c = c;
    }
  }

  public struct Z {
    public readonly double x, y;

    public Z(double x, double y) {
      this.x = x;
      this.y = y;
    }

    public double magSq => x * x + y * y;
    public double mag => Sqrt(x * x + y * y);

    #region equality

    public bool Equals(Z other) {
      return x.Equals(other.x) && y.Equals(other.y);
    }

    public override bool Equals(object obj) {
      if (ReferenceEquals(null, obj)) return false;
      return obj is Z && Equals((Z) obj);
    }

    public override int GetHashCode() {
      unchecked {
        return (x.GetHashCode() * 397) ^ y.GetHashCode();
      }
    }

    #endregion

    public static Z operator +(Z a, Z b) => new Z(a.x+b.x, a.y+b.y);
    public static Z operator +(Z a, int b) => new Z(a.x+b, a.y);
    public static Z operator -(Z a, Z b) => new Z(a.x-b.x, a.y-b.y);
    public static Z operator *(Z a, Z b) => new Z(a.x * a.x - b.y * b.y, a.x*b.y+a.y*b.x);

    public static Z operator ^(Z a, int b) {
      var zt = a;
      for (var i = 1; i < b; i++) {
        zt *= a;
      }
      return zt;
    }

    public static Z operator *(Z a, double b) => new Z(a.x*b, a.y*b);    
  }

  class Program {

    const int MAX_LOOP_NUMBER = 256;


    static void Main(string[] args) {
      var widthPx = 2048;
      var heigthPx = 1024;

      var xCenter = -0.5;
      var yCenter = 0.0;

      var width = 3.0;
      var heigth = 2.0;

      var xMin = xCenter - width / 2.0;
      var xDelta = width / widthPx;

      var yMix = yCenter - heigth / 2.0;
      var yDelta = heigth / heigthPx;

      var bit = new Bitmap(widthPx, heigthPx);

      var pixelInfo = Range(0, widthPx).AsParallel().SelectMany(
        xpx => Range(0, heigthPx).Select(
          ypx => new PixelInfo(
            xpx, 
            ypx, 
            stepsToColor(
              stepsToDivergate(
                new Z(xMin + xpx * xDelta, yMix + ypx * yDelta), 
                new Z(xMin + xpx * xDelta, yMix + ypx * yDelta)
              )
            )
          )
        )
      );

      foreach (var info in pixelInfo) {
        bit.SetPixel(info.x, info.y, info.c);
      }
      bit.Save("test.bmp");
    }

    public static Color stepsToColor(int steps) =>
      Color.FromArgb(steps % 256, steps % 256, steps % 256);
      

    public static int stepsToDivergate(Z z, Z c) {
      var n = 0;
      while (z.magSq < 4 && n < MAX_LOOP_NUMBER) {
        z = (z^2) + c;
        n++;
      }
      return n;
    }
  }
}
