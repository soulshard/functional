﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingPart
{
  class Program
  {
    static void Main(string[] args)
    {
    }
    
  }

  public delegate ComparisonResult compare<in A>(A a, A b);
  
  class IntComparer
  {
    public static IntComparer instantce = new IntComparer();
    
    public compare<int> compare => (a, b) =>
      a > b ? ComparisonResult.bigger
        : a < b ? ComparisonResult.smaller
        : ComparisonResult.equal;
  }

  class MyStringClass
  {
    public compare<B> Transform<A, B>(compare<A> copmparer, Func<B, A> fun) => (a, b) => copmparer(fun(a), fun(b));
   
    public compare<string> compare => Transform<int, string>(IntComparer.instantce.compare, a => a.Length);      
  }

  class MyStringClass2
  {
    public compare<B> Transform<A, B>(compare<A> copmparer, Func<B, A> fun) => (a, b) => copmparer(fun(a), fun(b));
    public compare<A> Invert<A>(compare<A> comp) => (a, b) => comp(b, a);
    public compare<string> compare => Invert(Transform<int, string>(IntComparer.instantce.compare, a => getSymbolCount(a)));
      //(a, b) => IntComparer.instantce.compare(getSymbolCount(a), getSymbolCount(b));

    int getSymbolCount(string from) =>
      from.Count(c => c == 'a');
  }

  public enum ComparisonResult : byte
  {
    equal,
    smaller,
    bigger
  }

}