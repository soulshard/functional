﻿//using System.Configuration;

using LaYumba.Functional;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Exercises.Chapter3 {
  public static class Exercises {
    // 1.  Write an overload of `GetOrElse`, that returns the wrapped value, or
    // calls a given function to compute an alternative default. What is the
    // benefit of this overload over the implementation we've seen above?

    public static A getOrElse<A>(this Option<A> @this, Func<A> ifNone) => @this.Match(ifNone, _ => _);

    // 2.  Write `Option.Map` in terms of `Match`.

    public static Option<B> map<A, B>(this Option<A> @this, Func<A, B> fun) => @this.Match(
      () => F.None,
      a => F.Some(fun(a))
    );

    // 3.  Write an extension method `Lookup` on `IDictionary<K, T>`, that
    // takes a `K` and returns an `Option<T>`.

    public static Option<T> lookUp<K, T>(this IDictionary<K, T> dic, K key) => dic.ContainsKey(key)
      ? F.Some(dic[key])
      : F.None;

    // 4.  Write an extension method `Map` on `IDictionary<K, T>`, that takes a
    // `Func<T, R>` and returns a new `Dictionary<K, R>`, mapping the original
    // keys to the values resulting from applying the function to the values.

    public static IDictionary<K, R> map<K, T, R>(this IDictionary<K, T> dic, Func<T, R> fun) =>
      dic.Select(pair => new KeyValuePair<K, R>(pair.Key, fun(pair.Value)))
         .ToDictionary(pair => pair.Key, pair => pair.Value);
  }

  // 6.  Write implementations for the methods in the `AppConfig` class
  // below. (For both methods, a reasonable one-line method body is possible.
  // Assume settings are of type string, numeric or date.) Can this
  // implementation help you to test code that relies on settings in a
  // `.config` file?
  public class AppConfig {
    NameValueCollection source;

    //public AppConfig() : this(ConfigurationManager.AppSettings) { }

    public AppConfig(NameValueCollection source) { this.source = source; }

    public Option<T> Get<T>(string name) => F.Some((T) Convert.ChangeType(source[name], typeof(T)));

    public T Get<T>(string name, T defaultValue) => Get<T>(name).Match(() => defaultValue, _ => _);
  }
}