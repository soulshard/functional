﻿using NUnit.Framework;
using System;
namespace Exercises.Chapter2 {
  // 1. Write a console app that calculates a user's Body-Mass Index:
  //   - prompt the user for her height in metres and weight in kg
  //   - calculate the BMI as weight/height^2
  //   - output a message: underweight(bmi<18.5), overweight(bmi>=25) or healthy weight
  // 2. Structure your code so that structure it so that pure and impure parts are separate
  // 3. Unit test the pure parts
  // 4. Unit test the impure parts using the HOF-based approach

  public enum WeightRange { underweight, healthy, overweight }

  public static class Bmi {

    public static void run() {
      main(readDouble, writeToConsole);
    }

    public static double bmi(double weigth, double height) =>
      Math.Round(weigth / (height * height), 2);

    public static WeightRange bmiToRange(this double bmi) =>
      bmi < 18.5 
        ? WeightRange.underweight 
        : bmi >= 25 
          ? WeightRange.overweight 
          : WeightRange.healthy; 

    public static void writeToConsole(WeightRange range) => 
      Console.WriteLine($"you are: {range}");

    public static double readDouble(string inputType) {
      double rez;
      Console.WriteLine($"Input {inputType}");
      while (!double.TryParse(Console.ReadLine(), out rez))
        Console.WriteLine($"That was not a numer. Input {inputType}");
      return rez;
    }

    public static void main(Func<string, double> read, Action<WeightRange> write) =>
      write(bmi(read("weight"), read("height")).bmiToRange());
  }

  public class BmiTests {
    [TestCase(1.7, 69, ExpectedResult = 23.88)]
    [TestCase(1.95, 125, ExpectedResult = 32.87)]
    public double CalculateBmi(double height, double weight)
       => Bmi.bmi(height, weight);

    [TestCase(20.33, ExpectedResult = WeightRange.healthy)]
    [TestCase(45.00, ExpectedResult = WeightRange.overweight)]
    public WeightRange ToBmiRange(double bmi) => bmi.bmiToRange();

    [TestCase(1.7, 69, ExpectedResult = WeightRange.healthy)]
    [TestCase(1.95, 125, ExpectedResult = WeightRange.overweight)]
    public WeightRange ReadBmi(double height, double weight) {
      var result = default(WeightRange);
      Func<string, double> read = s => s == "height" ? height : weight;
      Action<WeightRange> write = r => result = r;

      Bmi.main(read, write);
      return result;
    }
  }
}
