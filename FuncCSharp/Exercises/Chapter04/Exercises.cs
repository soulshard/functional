﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using LaYumba.Functional;
using LaYumba.Functional.Option;

namespace Exercises.Chapter4 {
  static class Exercises {
    // 1. Without looking at any code or documentation (or intllisense), write the function signatures of
    // `OrderByDescending`, `Take` and `Average`, which we used to implement `AverageEarningsOfRichestQuartile`:
    static decimal AverageEarningsOfRichestQuartile(List<Person> population)
      => population
        .OrderByDescending(p => p.Earnings)
        .Take(population.Count / 4)
        .Select(p => p.Earnings)
        .Average();

    // 2. Check your answer with the msdn documentation: https://msdn.microsoft.com/en-us/library/system.linq.enumerable(v=vs.110).aspx. 
    // How is `Average` different?

    // 3. Implement a general purpose `Compose` function that takes two unary functions
    // and returns the composition of the two.

    public static Func<A, C> compose<A, B, C>(this Func<A, B> funAB, Func<B, C> funBC) => a => funBC(funAB(a));

    // 4. Implement a `Lookup` extension method on `IDictionary<T>` returning `Option<T>`.

    public static Option<T> lookup<K, T>(this IDictionary<K, T> dic, K key) => dic.TryGetValue(key, out T val)
      ? F.Some(val)
      : F.None;

    // 5. Use `Bind` and the `Lookup` function from the previous exercise to
    // implement `GetWorkPermit` below.Then enrich the implementation so that `GetWorkPermit`
    // returns `None` if the work permit has expired.


    public static Option<WorkPermit> GetWorkPermit(Dictionary<string, Employee> people, string employeeId) =>
      people.lookup(employeeId)
        .Bind(emp => emp.WorkPermit);

    public static Option<WorkPermit> GetValidWorkPermit(Dictionary<string, Employee> people, string employeeId) =>
      people.lookup(employeeId)
        .Bind(emp => emp.WorkPermit)
        .Where(hasExpired.Negate());
    //.Where(permit => permit.Expiry.CompareTo(DateTime.Now) <= 0);

    public static Func<WorkPermit, bool> hasExpired => workPermit => workPermit.Expiry < DateTime.Now;



    // 6. Use `Bind` to implement `AverageYearsWorkedAtTheCompany` below(only
    // employees who have left should be included).

    public static double AverageYearsWorkedAtTheCompany(List<Employee> employees) =>
      employees.Bind(employee => employee.LeftOn.Map(left => YearsBetween(employee.JoinedOn, left)))
        .Average();

    static double YearsBetween(DateTime start, DateTime end)
      => (end - start).Days / 365d;

    // 7. Write implementations of `Where`, `ForEach` and `Bind` for
    // `Container`. Try doing so without looking at the implementations for
    // `Option` first, and only check them if needed.

    public static Container<R> Map<T, R>(this Container<T> cont, Func<T, R> fun) => new Container<R>(fun(cont.value));
    //imposiblue
    //public static Container<T> Where<T>(this Container<T> cont, Predicate<T> predecate) =>
    public static Container<R> Bind<T, R>(this Container<T> cont, Func<T, Container<R>> fun) => fun(cont.value);
    public static Unit forEach<T>(this Container<T> cont, Func<T, Unit> fun) => fun(cont.value);
    // public static Unit ForEach ...
  }

  public struct Container<T> {
    public readonly T value;
    public Container(T value) { this.value = value; }
  }

  public struct WorkPermit {
    public string Number { get; set; }
    public DateTime Expiry { get; set; }
  }

  public class Employee {
    public string Id { get; set; }
    public Option<WorkPermit> WorkPermit { get; set; }

    public DateTime JoinedOn { get; }
    public Option<DateTime> LeftOn { get; }
  }
}