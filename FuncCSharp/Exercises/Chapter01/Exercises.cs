﻿using System;
using System.Collections.Generic;
using System.Linq;
using LaYumba.Functional;

namespace Exercises.Chapter1 {
  static class Exercises {

    // 1. Write a function that negates a given predicate: whenvever the given predicate
    // evaluates to `true`, the resulting function evaluates to `false`, and vice versa.

    public static Predicate<A> Negate<A>(this Predicate<A> pred) => a => !pred(a);

    // 2. Write a method that uses quicksort to sort a `List<int>` (return a new list,
    // rather than sorting it in place).

    //public static List<int> quickSort(this List<int> toSort) {
    //  if (toSort.Count == 0) return new List<int>();
    //  var pivot = toSort.Last();
    //  var a = toSort.Where(e => e <= pivot).ToList();
    //  var b = toSort.Where(e => e > pivot).ToList();
    //  return a.quickSort().Concat(b.quickSort()).ToList();
    //}

    // 3. Generalize your implementation to take a `List<T>`, and additionally a 
    // `Comparison<T>` delegate.

    public static List<T> quickSort<T>(this List<T> toSort, Comparison<T> compare) {
      if (toSort.Count == 0) return new List<T>();
      var pivot = toSort.Last();
      var a = toSort.Where(e => compare(e, pivot) <= 0).ToList();
      var b = toSort.Where(e => compare(e, pivot) > 0).ToList();
      return a.quickSort(compare).Concat(b.quickSort(compare)).ToList();
    }

    // 4. In this chapter, you've seen a `Using` function that takes an `IDisposable`
    // and a function of type `Func<TDisp, R>`. Write an overload of `Using` that
    // takes a `Func<IDisposable>` as first
    // parameter, instead of the `IDisposable`. (This can be used to fix warnings
    // given by some code analysis tools about instantiating an `IDisposable` and
    // not disposing it.)

    // Using
    public static R Using<TDisp, R>(Func<TDisp> disposable
      , Func<TDisp, R> func) where TDisp : IDisposable {
      using (var disp = disposable()) return func(disp);
    }

  }
}
