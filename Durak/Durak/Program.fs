﻿open System.IO

type Suit = Spade = 0 | Diamond = 1 | Clubs = 2 | Hearts = 3

type Value = Two = 0 | Three = 1 | Four = 2 | Five = 3 
            | Six = 4 | Seven = 5 | Eigth = 6 | Nine = 7 
            | Ten = 8 | Jack = 9 | Queen = 10 | King = 11 | Ace =12

type Card = {value : Value; suit : Suit}

type CardPair = {first : Card; second : Card option} 

type Player = {playerCards : list<Card>; number : int}

type Table = {tableCardPairs : list<CardPair>}

type GameState = {offense : Player; defence : Player; table:Table; trump:Suit}

let CompareCard x y trump = 
  match y with
  | y when y.value < x.value || (y.suit = trump && x.suit <> trump) -> 1
  | y when y.value > x.value || (y.suit <> trump && x.suit = trump)-> -1
  | _ -> 0

let CompareCardWithSuit x y = 
  match y with
  | y when y.value < x.value && y.suit = x.suit -> 1
  | y when y.value > x.value && y.suit = x.suit -> -1
  | _ -> 0

let CardsEquals x y =
  match y with
  | y when y.value = x.value && y.suit = x.suit -> true
  | _ -> false

let GetSmallesCard cards trump  =
  cards |> List.fold (fun acc elem -> if (CompareCard elem acc trump  < 0 ) then elem else acc) cards.Head

let GetCardsOfTheSameSuit cards suit =
  cards |> List.filter (fun elem -> elem.suit = suit)

let GetCardsBiggerThen cards value =
  cards |> List.filter (fun elem -> elem.value > value)

let GetCardsOfTheSameRank cards rank trump =
  let rank = cards |> List.filter(fun elem -> elem.value = rank)
  if (rank.Length = 0) then None
  elif (rank.Length > 1) then Some(rank |> List.filter(fun elem -> elem.suit <> trump))
  else Some(rank)
  
let PlayCard player card =
  { playerCards = player.playerCards |> List.filter(fun elem -> (not (CardsEquals elem card))); number =  player.number}
  
let IsCardValueInCardPairList card cardPairList =
  not (cardPairList |> List.filter(fun cardPair -> 
        cardPair.first.value = card.value || (cardPair.second.IsSome && cardPair.second.Value.value = card.value)
      )).IsEmpty

let GetOffensiveCard cardsOnTheTable playerCards trump =
  let rez = playerCards |> List.filter(fun elem -> IsCardValueInCardPairList elem cardsOnTheTable)
  if (rez.IsEmpty) then None
  else Some(GetSmallesCard rez trump)

let GetDefensiveCard cards againstCard trump =
  let sameSuit = GetCardsBiggerThen (GetCardsOfTheSameSuit cards againstCard.suit) againstCard.value
  if (sameSuit.IsEmpty) then 
    if againstCard.suit <> trump then
      let trumps = GetCardsOfTheSameSuit cards trump
      if trumps.IsEmpty then None
      else Some(GetSmallesCard trumps trump)
    else None
  else Some(GetSmallesCard sameSuit trump)

let AddCardPairToTable table cardPair =
  { tableCardPairs = cardPair :: table.tableCardPairs }

let AddCardToTable table card =
  AddCardPairToTable table { first = card ; second = None}

let RemoveCardPair cards cardPair =
  cards |> List.filter (fun x -> x <> cardPair) 

let GetEmptyPairs gameState =
  gameState.table.tableCardPairs |> List.filter (fun x -> x.second.IsNone) 

let AddCardToCardPair cards cardPair card =
  {tableCardPairs = { first = cardPair.first; second = Some(card) } :: RemoveCardPair cards cardPair}

let FlattenCardPair cardPair = 
  match cardPair.second with
  | Some value -> [cardPair.first; value]
  | None -> [cardPair.first]

let GetAllTableCards table =
  table.tableCardPairs |> List.fold (fun acc card -> FlattenCardPair card |> List.append acc ) []

let GiveCardsToDefensePlayer gameState = {
  offense = gameState.offense; 
  defence = {playerCards = GetAllTableCards gameState.table |> List.append gameState.defence.playerCards; 
  number = gameState.defence.number}; 
  table = gameState.table;
  trump = gameState.trump
}  

let ClearTheTable gameState = {
  offense = gameState.offense; 
  defence = gameState.defence; 
  table = {tableCardPairs=[]}; 
  trump = gameState.trump
}  

let SwitchPlayers gameState = {
  offense = gameState.defence; 
  defence = gameState.offense; 
  table = gameState.table;
  trump = gameState.trump
}  

let CheckTheRoundWinnerAndClearTable gameState =
  if (GetEmptyPairs gameState).IsEmpty then
    gameState |> ClearTheTable |> SwitchPlayers 
  else    
    gameState |> GiveCardsToDefensePlayer |> ClearTheTable

let rec AttackAndDefendTillAble gameState addedMoreAttackCards =
  if gameState.defence.playerCards.Length > gameState.table.tableCardPairs.Length then
    let offensiveCard = GetOffensiveCard gameState.table.tableCardPairs gameState.offense.playerCards gameState.trump
    if offensiveCard.IsSome then 
      AttackAndDefendTillAble  {
        offense =  PlayCard gameState.offense offensiveCard.Value ; 
        defence = gameState.defence; 
        table =  AddCardToTable gameState.table offensiveCard.Value;
        trump  = gameState.trump
      } true
    elif addedMoreAttackCards then DefendAndAttackTillAble gameState
    else gameState 
  else
    if addedMoreAttackCards then DefendAndAttackTillAble gameState
    else gameState 

and DefendAndAttackTillAble gameState = 
  let undefendedCards = GetEmptyPairs gameState
  if undefendedCards.IsEmpty then AttackAndDefendTillAble gameState false
  else
    let def = GetDefensiveCard gameState.defence.playerCards undefendedCards.Head.first gameState.trump
    if def.IsNone then AttackAndDefendTillAble gameState false
    else
      DefendAndAttackTillAble {
        offense = gameState.offense; 
        defence = PlayCard gameState.defence def.Value; 
        table =  AddCardToCardPair gameState.table.tableCardPairs undefendedCards.Head def.Value;
        trump  = gameState.trump
      }

let rec DefenseTriesToFlipTheAttack gameState = 
  if gameState.offense.playerCards.Length > gameState.table.tableCardPairs.Length then 
    let cards = GetCardsOfTheSameRank gameState.defence.playerCards gameState.table.tableCardPairs.Head.first.value gameState.trump
    if cards.IsNone then gameState
    else 
      let cardToPlay = cards.Value.Head
      DefenseTriesToFlipTheAttack {
        offense = PlayCard gameState.defence cardToPlay; 
        defence = gameState.offense; 
        table =  AddCardToTable gameState.table cardToPlay;
        trump  = gameState.trump
      }
  else 
    gameState
  
let OffencePlaysSmallestCard gameState =
  let smallCard = GetSmallesCard gameState.offense.playerCards gameState.trump
  { offense = PlayCard gameState.offense smallCard; 
    defence = gameState.defence; 
    table =  AddCardToTable gameState.table smallCard;
    trump = gameState.trump
  }  

let rec PlayoutHand gameState count = 
  if (not gameState.offense.playerCards.IsEmpty && not gameState.defence.playerCards.IsEmpty) then
    PlayoutHand (gameState |> OffencePlaysSmallestCard |> DefenseTriesToFlipTheAttack |> DefendAndAttackTillAble |> CheckTheRoundWinnerAndClearTable) count+1
  else
    if gameState.offense.playerCards.IsEmpty && gameState.defence.playerCards.IsEmpty then 0
    elif gameState.offense.playerCards.IsEmpty then gameState.offense.number
    else  gameState.defence.number

let trump = Suit.Diamond
let gameState = {
  offense = {playerCards = [{value = Value.Ace; suit = Suit.Diamond}]; number = 1};
  defence = {playerCards = [{value = Value.Ace; suit = Suit.Diamond}]; number = 2};
  table = {tableCardPairs = []};
  trump = trump
}
//PlayoutHand gameState 0

let parseSuit s = 
    match s with
    | 'D' -> Some(Suit.Diamond)
    | 'S' -> Some(Suit.Spade)
    | 'C' -> Some(Suit.Clubs)
    | 'H' -> Some(Suit.Hearts)
    | _ -> None

let parseValue v =
    match v with
    | '2' -> Some(Value.Two)
    | '3' -> Some(Value.Three)
    | '4' -> Some(Value.Four)
    | '5' -> Some(Value.Five)
    | '6' -> Some(Value.Six)
    | '7' -> Some(Value.Seven)
    | '8' -> Some(Value.Eigth)
    | '9' -> Some(Value.Nine)
    | 'T' -> Some(Value.Ten)
    | 'J' -> Some(Value.Jack)
    | 'Q' -> Some(Value.Queen)
    | 'K' -> Some(Value.King)
    | 'A' -> Some(Value.Ace)
    | _ -> None

let parseCard s =
    if s.Length > 2 && s.Length < 1 then None
    else
        let suitOpt = parseSuit s.Chars(0)
        let valueOpt = parseValue s.Chars(1)
        if suitOpt.IsNone && valueOpt.IsNone then None
        else Some({value = valueOpt.Value; suit = suitOpt.Value})

let rec parseHand s =
    let handSoFar = []
    if s.Length > 2 then List.append handSoFar (parseHand s.SubString(2))
    else 
        let card = parseCard s
        if card.IsSome then card.Value :: handSoFar
        else handSoFar

let parseStartingGameState s trump =
    let cards = s.Split('|')
    let player1Cards = parseHand cards.Head.Split(' ')
    let player2Cards = parseHand cards.Tail.Split(' ')
    {offense = {playerCards = player1Cards; number = 1}; 
    defence ={playerCards = player2Cards; number = 2}; 
    table =  {tableCardPairs = []}; 
    trump = trump}

let printGameEnds winner =   
  ()
  
let stream = new StreamReader("example-data1.txt")
let trumpSuit = stream.ReadLine().Chars(0) |> parseSuit
let allCards = stream.ReadToEnd().Split('\n')
allCards |> List.iter (fun x -> printGameEnds (parseStartingGameState x trumpSiut.Value))

